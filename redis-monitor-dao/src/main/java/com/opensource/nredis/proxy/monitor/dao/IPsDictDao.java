package com.opensource.nredis.proxy.monitor.dao;

import com.opensource.nredis.proxy.monitor.model.PsDict;


/**
* dao
*
* @author liubing
* @date 2016/12/21 17:32
* @version v1.0.0
*/
public interface IPsDictDao extends IMyBatisRepository<PsDict>,IPaginationDao<PsDict> {
}
