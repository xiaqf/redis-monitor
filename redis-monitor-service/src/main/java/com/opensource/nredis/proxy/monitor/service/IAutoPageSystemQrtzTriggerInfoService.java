package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.AutoPageSystemQrtzTriggerInfo;

/**
* service interface
*
* @author liubing
* @date 2016/12/30 08:41
* @version v1.0.0
*/
public interface IAutoPageSystemQrtzTriggerInfoService extends IBaseService<AutoPageSystemQrtzTriggerInfo>,IPaginationService<AutoPageSystemQrtzTriggerInfo>  {
}

