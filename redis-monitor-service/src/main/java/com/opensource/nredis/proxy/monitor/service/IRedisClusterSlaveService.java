package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.RedisClusterSlave;

/**
* service interface
*
* @author liubing
* @date 2017/01/11 12:18
* @version v1.0.0
*/
public interface IRedisClusterSlaveService extends IBaseService<RedisClusterSlave>,IPaginationService<RedisClusterSlave>  {
}

